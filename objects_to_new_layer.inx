<?xml version="1.0" encoding="UTF-8"?>


<!--Copyright (C) [2021] [Matt Cottam], [mpcottam@raincloud.co.uk]-->

<!--        This program is free software; you can redistribute it and/or modify-->
<!--        it under the terms of the GNU General Public License as published by-->
<!--        the Free Software Foundation; either version 2 of the License, or-->
<!--        (at your option) any later version.-->

<!--        This program is distributed in the hope that it will be useful,-->
<!--        but WITHOUT ANY WARRANTY; without even the implied warranty of-->
<!--        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the-->
<!--        GNU General Public License for more details.-->

<!--        You should have received a copy of the GNU General Public License-->
<!--        along with this program; if not, write to the Free Software-->
<!--        Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.-->


<!--        #############################################################################-->
<!--        #  #  Objects To New Layer - Transfer or Copy Selected Objects to New Layer   -->
<!--        #  After setting the options in the main dialogue-->
<!--        #  Assign a shortcut in Inkscape Edit>Preferences>Interface>Keyboard to org.inkscape.inklinea.objects_to_new_layer.noprefs -->
<!--        #  For shortcut triggered objects to new layer with last settings-->
<!--        # Requires Inkscape 1.1+ -->
<!--        #############################################################################-->

<inkscape-extension xmlns="http://www.inkscape.org/namespace/inkscape/extension">
    <name>Objects To New Layers</name>
    <id>org.inkscape.inklinea.objects_to_new_layer</id>

    <param name="objects_to_new_layer_notebook" type="notebook">

    <page name="settings_page" gui-text="Settings">
        <hbox>
            <param name="layer_prefix_string" type="string" gui-text="Layer Name Prefix">New_Layer</param>
        </hbox>
        <hbox>
            <param name="method_radio" type="optiongroup" appearance="radio" gui-text="Method">
                <option value="transfer">Transfer Objects</option>
                <option value="copy">Copy Objects</option>

            </param>

        </hbox>
        <hbox>
            <param name="suffix_radio" type="optiongroup" appearance="radio" gui-text="Suffix Type">
                <option value="epoch">Epoch Time</option>
                <option value="random">Random</option>


            </param>

        </hbox>
        <hbox>
          <param name="treat_groups_as_objects" type="boolean" gui-text="Treat groups as objects" gui-description="Maintain groups when moving/copying to new layer">false</param>

        </hbox>
        <hbox>
            <param name="separate_layers_cb" type="boolean" gui-text="Separate Layers" gui-description="Create new layer for each selected object">false</param>

        </hbox>
        <hbox>
            <param name="nested_layers_cb" type="boolean" gui-text="Nest Layers" gui-description="Nest new layers(s) in a new layer">true</param>

        </hbox>

        <hbox>

            <label xml:space="preserve">
    ▶ Can be used from the command line

		</label>

        </hbox>
    </page>

    <page name="about_page" gui-text="About">

        <label>
            Objects To New Layer - An Inkscape Extension
        </label>
        <label>
            Inkscape 1.1 +
        </label>
        <label appearance="url">

            https://inkscape.org/~inklinea/resources/=extension/

        </label>

        <label appearance="url">

            https://gitlab.com/inklinea

        </label>

        <label xml:space="preserve">
▶ Creates a new layer and copies or
  transfers the selected objects to
  that layer
▶ Appears in 'Extensions>Arrange'
▶ A shortcut can be used to
  org.inkscape.inklinea.objects_to_new_layer.noprefs

		</label>
    </page>


    </param>


    <effect>
        <object-type>path</object-type>
        <effects-menu>
            <submenu name="Arrange">
            <submenu name="Objects To New Layer"></submenu>
            </submenu>
        </effects-menu>
    </effect>
    <script>
        <command location="inx" interpreter="python">py/objects_to_new_layer.py</command>
    </script>
</inkscape-extension>

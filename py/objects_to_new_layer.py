#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2021] [Matt Cottam], [mpcottam@raincloud.co.uk]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#
# #############################################################################
#  Objects To New Layer - Transfer or Copy Selected Objects to New Layer
#  After setting the options in the main dialogue
#  Assign a shortcut in Inkscape Edit>Preferences>Interface>Keyboard to org.inkscape.inklinea.objects_to_new_layer.noprefs
#  For shortcut triggered objects to new layer with last settings
#  Requires Inkscape 1.1+ -->
# #############################################################################

import inkex

from inkex import Group

import random, time

## Global variables
global layer_list
layer_list = []

def create_new_group(self, prefix, mode, suffix_type='epoch_time'):

    suffix_type = self.options.suffix_radio

    if suffix_type == 'random':
        id_suffix = str(random.randrange(1000000, 9999999))
    if suffix_type == 'epoch':
        id_suffix = str(time.time())

    group_id = str(prefix) + '_' + id_suffix
    new_group = self.svg.add(Group.new(group_id))
    new_group.set('inkscape:groupmode', str(mode))
    new_group.attrib['id'] = group_id

    return new_group


def add_objects_to_new_layer(self, objects, layer_name_prefix, method):
    my_layer = create_new_group(self, layer_name_prefix, 'layer')

    if method == 'transfer':
        # Add to global layer list
        layer_list.append(my_layer.get_id())

        if self.options.treat_groups_as_objects != 'true' and len(objects) > 0:
            for my_object in objects:
                my_object_composed_transform = my_object.composed_transform()
                my_layer.append(my_object)
                my_object.transform = my_object_composed_transform
        else:
                objects_composed_transform = objects.composed_transform()
                my_layer.append(objects)
                objects.transform = objects_composed_transform

    if method == 'copy':
        # Add to global layer list
        layer_list.append(my_layer.get_id())
        if self.options.treat_groups_as_objects != 'true' and len(objects) > 0:
            for my_object in objects:
                my_copied_object = my_object.duplicate()
                my_layer.append(my_copied_object)
                my_copied_object.transform = my_object.composed_transform()
        else:
            my_copied_object = objects.duplicate()
            my_layer.append(my_copied_object)
            my_copied_object.transform = objects.composed_transform()


def objects_to_separate_layers(self, objects, layer_name_prefix, method):
    for my_object in objects:
        add_objects_to_new_layer(self, my_object, layer_name_prefix, method)


def nest_layers(self, layer_list):
    nest_layer = create_new_group(self, 'Nested', 'layer')
    for layer_id in layer_list:
        my_layer = self.svg.getElementById(layer_id)
        nest_layer.append(my_layer)

def relabel_layers(self, layer_list):

    for layer_id in layer_list:
        my_layer = self.svg.getElementById(layer_id)
        # my_layer.set('inkscape:label', my_layer.get_id())
        my_layer.set('inkscape:label', 'pod')


class ObjectsToNewLayer(inkex.EffectExtension):

    def add_arguments(self, pars):

        pars.add_argument("--objects_to_new_layer_notebook", type=str, dest="objects_to_new_layer_notebook", default=0)

        pars.add_argument("--method_radio", type=str, dest="method_radio", default='transfer')

        pars.add_argument("--suffix_radio", type=str, dest="suffix_radio", default='epoch')

        pars.add_argument("--layer_prefix_string", type=str, dest="layer_prefix_string", default='x')

        pars.add_argument("--separate_layers_cb", type=str, dest="separate_layers_cb", default='false')

        pars.add_argument("--nested_layers_cb", type=str, dest="nested_layers_cb", default='true')

        pars.add_argument("--treat_groups_as_objects", type=str, dest="treat_groups_as_objects", default='false')

    def effect(self):

        my_objects = self.svg.selected

        layer_name_prefix = str(self.options.layer_prefix_string)

        if layer_name_prefix == '':
            layer_name_prefix = 'New_Layer'

        method = str(self.options.method_radio)

        if len(my_objects) > 0:

            if self.options.separate_layers_cb == 'false':
                add_objects_to_new_layer(self, my_objects, layer_name_prefix, method)
            if self.options.separate_layers_cb == 'true':
                objects_to_separate_layers(self, my_objects, layer_name_prefix, method)
        else:
            return

        if self.options.nested_layers_cb == 'true':
            nest_layers(self, layer_list)

if __name__ == '__main__':
    ObjectsToNewLayer().run()
